#DEPENDENCIES

BUILD_DIR = build
SRC_DIR = src
LIBS_DIR = lib

CXX ?= g++
CFLAGS += -c -Os -Wall -Wextra -Wold-style-cast -Woverloaded-virtual -std=c++11 -Iinclude
LDFLAGS += -Os -flto
LDFLAGS += -L$(LIBS_DIR)/ -lmongoose -lserial -pthread

SOURCES_RAW = main.cpp Printer.cpp util.cpp FileManager.cpp
SOURCES = $(addprefix $(SRC_DIR)/,$(SOURCES_RAW))

OBJECTS_RAW = $(SOURCES_RAW:.cpp=.o)
OBJECTS = $(addprefix $(BUILD_DIR)/,$(OBJECTS_RAW))

EXECUTABLE = $(BUILD_DIR)/server

DIRS = $(sort $(dir $(OBJECTS)))

MKDIR_PREFIX = mkdir -p

all: run

clean:
	rm -f $(EXECUTABLE) $(OBJECTS)

run: build_all
	-@$(MKDIR_PREFIX) $(BUILD_DIR)/GCODES
	./$(EXECUTABLE) /dev/ttyUSB0 115200 8080 $(BUILD_DIR)/GCODES

$(EXECUTABLE): $(OBJECTS)
	$(CXX) $(OBJECTS) -o $@ $(LDFLAGS)

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CXX) $(CFLAGS) $< -o $@

$(DIRS):
	-@$(MKDIR_PREFIX) $(DIRS)

get_libs: lib/libmongoose.a lib/libserial.a

lib/libserial.a:
	mkdir -p lib
	mkdir -p include
	git clone git@bitbucket.org:skeen/serial-builder.git
	make -C serial-builder
	cp serial-builder/release/libserial.a lib/libserial.a
	cp -r serial-builder/release/serial include/
	rm -rf serial-builder

lib/libmongoose.a:
	mkdir -p lib
	mkdir -p include
	git clone git@bitbucket.org:skeen/mongoose-builder.git
	make -C mongoose-builder
	make -C mongoose-builder
	cp mongoose-builder/release/libmongoose.a lib/libmongoose.a
	mkdir -p include/mongoose
	cp -r mongoose-builder/release/include/* include/mongoose/
	cp mongoose-builder/release/mongoose.h include/mongoose.h
	rm -rf mongoose-builder

build_all: get_libs $(DIRS) $(SOURCES) $(EXECUTABLE)
