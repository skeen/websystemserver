#include "FileManager.hpp"

FileManager::FileManager(std::string gcode_path)
    : gcode_path(gcode_path)
{
}

std::string FileManager::handle(std::string gcode_str)
{
    // If we're in save mode, consume anything!
    if(save_mode)
    {
        return save_gcode(gcode_str);
    }

    // We only handle M codes, not an M code, not for us
    if(gcode_str[0] != 'M')
        return KO;

    // Find the gcode number itself
    uint32_t gcode = str_to_int(gcode_str);
    // Switch and do what's required of us
    switch(gcode)
    {
            // M20: List SD card
        case 20:
            return list_files();

            // M21: Initialize SD card
            // M22: Release SD card
        case 21:
        case 22:
            return OK;

            // M23: Select SD file
        case 23:
            return select_file(gcode_str);

            // M24: Start/resume SD print
        case 24:
            // TODO: Check file mode
            // TODO: Read file from another thread?
            return NOT_IMPLEMENTED;

            // M25: Pause SD print
        case 25:
            // TODO: See above
            return NOT_IMPLEMENTED;

            // M26: Set SD position
        case 26:
            return NOT_SUPPORTED;

            // M27: Report SD print status
        case 27:
            // TODO: Write information about the storage
            return NOT_IMPLEMENTED;

            // M28: Begin write to SD card
        case 28:
            return write_file(gcode_str);

            // M29: Stop writing to SD card
        case 29:
            // NOTE: This stop case, is handled within save()
            // This is only hit, if we fire M29 without being in save mode.
            return "ERROR: NO WRITING ACTIVE";

            // M30: Delete a file on the SD card
        case 30:
            return delete_file(gcode_str);

            // M31: Output time since last M109
            // or SD card start to serial
        case 31:
            return NOT_SUPPORTED;

            // M32: Select file and start SD print
        case 32:
            // TODO: Combine M23 + M24
            return NOT_IMPLEMENTED;

            // Not an SD card Gcode!
        default:
            return KO;
    }
}

uint32_t FileManager::str_to_int(std::string gcode_str)
{
    // Break away the 'M' (start at 1)
    // Remove all but the following 2 digits (size 2)
    const std::string& digits = gcode_str.substr(1,2);
    // Lexical cast to a number and return it
    return lexical_cast<uint32_t>(digits);
}

std::string FileManager::str_to_argument(std::string gcode_str)
{
    std::size_t T_start = gcode_str.find_first_of(" ");
    gcode_str.erase(0, T_start);
    return gcode_str;
}

std::string FileManager::str_to_filepath(std::string gcode_str)
{
    std::string filename = str_to_argument(gcode_str);
    std::string combined = gcode_path;
    combined += "/";
    combined += trim(filename);
    return combined;
}

bool FileManager::file_exists(std::string filename)
{
    std::ifstream infile(filename);
    return infile.good();
}

#include <dirent.h>
std::string FileManager::list_files()
{
    // Open a directory for reading
    DIR* dir = opendir(gcode_path.c_str());
    if(dir != NULL)
    {
        std::string result = "ok Files: {";
        // Loop through all the entities of the directory
        struct dirent* ent = nullptr;
        while ((ent = readdir(dir)) != NULL)
        {
            // Append the name of this to our result
            std::string filename = ent->d_name;
            result += filename;
            result += ",";
        }
        // Get rid of the last superfluous comma
        // TODO: Fix this, we need to remove, but this break if there're no elements at all
        //result = result.substr(0, result.length()-1);
        // Append the ending to our result
        result += "}\n";
        // Close the directory
        closedir(dir);
        return result;
    }
    else
    {
        std::string error = "ERROR: UNABLE TO OPEN DIRECTORY; ";
        error += gcode_path;
        return error;
    }
}

std::string FileManager::select_file(std::string gcode_str)
{
    std::string combined = str_to_filepath(gcode_str);

    if(file_exists(combined) == false)
    {
        std::string error = "ERROR: NO SUCH FILE; ";
        error += combined;
        return error;
    }

    // Close if we happen to be open
    if(current.is_open()) current.close();
    current.open(combined, std::fstream::in);
    return OK;
}

std::string FileManager::delete_file(std::string gcode_str)
{
    std::string combined = str_to_filepath(gcode_str);

    if(file_exists(combined) == false)
    {
        std::string error = "ERROR: NO SUCH FILE; ";
        error += combined;
        return error;
    }

    int ret_code = std::remove(combined.c_str());
    if (ret_code == 0)
    {
        return OK;
    }
    else
    {
        std::string error = "ERROR: UNABLE TO DELETE; ";
        error += combined;
        return error;
    }
}

std::string FileManager::write_file(std::string gcode_str)
{
    std::string combined = str_to_filepath(gcode_str);

    // Close if we happen to be open
    if(current.is_open()) current.close();
    current.open(combined, std::fstream::out | std::fstream::trunc);

    if(current.good() == false)
    {
        std::string error = "ERROR: UNABLE TO OPEN (writing); ";
        error += combined;
        return error;
    }

    save_mode = true;
    return OK;
}

std::string FileManager::save_gcode(std::string gcode_str)
{
    // Handle M29: Stop writing to SD card
    uint32_t gcode = str_to_int(gcode_str);
    if(gcode_str[0] == 'M' && gcode == 29)
    {
        // TODO: Check that the file argument fits the currently open file
        current.close();
        save_mode = false;
        return OK;
    }
    // Append the gcode to the output
    current << gcode_str << std::endl;

    return "CONSUMED";
}
