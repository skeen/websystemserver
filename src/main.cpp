#include "util.hpp"

#include "Printer.hpp"
#include "FileManager.hpp"

#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <mongoose/Server.h>
#include <mongoose/WebController.h>

class PrinterController : public Mongoose::WebController
{
    private:
        Printer print;
        FileManager fm;
        
    public:
        PrinterController(std::string tty, uint32_t baudrate, std::string gcode_path)
            : print(tty, baudrate), fm(gcode_path)
        {
        }
    
    public: 
        // TODO: Remove this, is parsing is moved to javascript
        void temp(Mongoose::Request& /*request*/, Mongoose::StreamResponse& response)
        {
            response << map_to_json(print.m105()) << std::endl;

            response.setHeader("Content-Type", "application/json");
        }

        void gcode(Mongoose::Request& request, Mongoose::StreamResponse& response)
        {
            response.setHeader("Access-Control-Allow-Origin", "*");

            //bool verbose = lexical_cast<bool>(request.get("verbose", "1"));
            std::string str = request.get("gcode");
            if(str.length() == 0)
            {
                response << "Invalid! No gcode url-argument provided!" << std::endl;
                return;
            }
#ifdef PROTOCOL_DEBUG
            response << "You sent the gcode: " << str << "<br/>" << std::endl;
            response << "Response: " << print.raw_gcode(str) << std::endl;
#else // PROTOCOL_DEBUG
            // Let the file manager handle the gcode if it needs to, otherwise
            // let the printer handle it.
            // TODO: We need an instruction queue, and an ability to hook into it
            std::string res = fm.handle(str);
            if(res != "")
                response << res << std::endl;
            else
                response << print.raw_gcode(str) << std::endl;
#endif // PROTOCOL_DEBUG
        }

        void index(Mongoose::Request& /*request*/, Mongoose::StreamResponse& response)
        {
            response << "<h1>Welcome to the <b>/dev/wifi</b> machine server</h1>"
                     << "This server is this binding connection between the webinterface"
                     << " and the actual machine.<br/><br/>"
                     << "This server exposes a RESTful API:"
                     << "<ul>"
                     << "<li><a href='/'>This page</a></li>"
                     << "<li><a href='/temp'>Temperature JSON</a></li>"
                     << "</ul>" << std::endl;
        }

        void setup()
        {
            using namespace Mongoose;
            addRoute("GET", "/gcode", PrinterController, gcode);
            addRoute("GET", "/temp", PrinterController, temp);
            addRoute("GET", "/", PrinterController, index);
        }
};

int main(int argc, char* argv[])
{
    if(argc < 5)
    {
        std::cout << "Usage: server /dev/tty??? BAUDRATE webport gcode_folder" << std::endl;
        return -1;
    }

    std::string tty(argv[1]);
    uint32_t baudrate = lexical_cast<uint32_t>(argv[2]);
    uint32_t port = lexical_cast<uint32_t>(argv[3]);
    std::string gcode_folder(argv[4]);

    PrinterController controller(tty, baudrate, gcode_folder);
    Mongoose::Server server(port);
    server.registerController(&controller);
    server.start(); 
    std::cout << "Hosting at localhost:" << port << std::endl;

    while(1)
    {
        // TODO: Have main thread run gcodes?
        std::cout << "." << std::flush;
        sleep(1);
    }
}
