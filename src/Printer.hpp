#ifndef PRINTER_HPP
#define PRINTER_HPP

#include <map>

#include "serial/serial.h"

class Printer
{
    private:
        serial::Serial serial;

    private:

    public:
        Printer(std::string port = "/dev/ttyUSB0", uint32_t baudrate = 115200);

        std::map<std::string, std::string> m105();
        std::string raw_gcode(std::string gcode);
};

#endif //PRINTER_HPP
