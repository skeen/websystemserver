#include "Printer.hpp"

#include "util.hpp"

#include <cassert>
#include <iostream>

Printer::Printer(std::string port, uint32_t baudrate)
    : serial(port, baudrate, serial::Timeout::simpleTimeout(1000))
{
    while(serial.waitReadable() != false)
        serial.readline();
}

std::map<std::string, std::string> Printer::m105()
{
    serial.write("M105\n");
    serial.waitReadable();
    std::string str = serial.readline();

    auto exists = [](std::string str, std::string tag) -> bool
    {
        std::size_t T_start = str.find(tag);
        return (T_start != std::string::npos);
    };

    auto find_and_get = [](std::string str, std::string tag) -> std::string
    {
        std::size_t T_start = str.find(tag);
        assert(T_start != std::string::npos);
        str.erase(0, T_start);
        std::size_t T_end = str.find(" ");
        assert(T_end != std::string::npos);
        std::string T = str.substr(tag.length(), T_end-tag.length());
        str.erase(0, T_end+tag.length());
        return T;
    };

    std::string T = find_and_get(str, "T:");
    //lexical_cast<float>(find_and_get(str, "T:"));
    std::string B = find_and_get(str, "B:");
    //lexical_cast<float>(find_and_get(str, "B:"));

    std::map<std::string, std::string> temp;
    temp["T"] = T;
    temp["B"] = B;

    for(unsigned int x = 0;;x++)
    {
        std::string tag = "T";
        tag += std::to_string(x);
        tag += ":";

        if(exists(str,tag) == false)
            break;

        std::string t_temp = find_and_get(str, tag);
        //lexical_cast<float>(find_and_get(str, tag));
        temp[tag.substr(0,tag.length()-1)] = t_temp;
    }

    return temp;
}

std::string Printer::raw_gcode(std::string gcode)
{
    gcode += "\n";
    serial.write(gcode);
    serial.waitReadable();
    return serial.readline();
}
