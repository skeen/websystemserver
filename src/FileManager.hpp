#ifndef FILEMANAGER_HPP
#define FILEMANAGER_HPP

#include "util.hpp"

#include <string>
#include <fstream>
#include <cassert>

class FileManager
{
    private:
        bool save_mode = false;
        const std::string gcode_path;
        // Shared for printing and saving
        std::fstream current;

        // String constants
        const std::string OK = "ok\n";
        const std::string KO = "";
        // TODO: Consider if we want to support these things
        const std::string NOT_SUPPORTED = "NOT SUPPORTED, SORRY!\n";
        const std::string NOT_IMPLEMENTED = "NOT IMPLEMENTED, SORRY!\n";

    public:
        FileManager(std::string gcode_path);
        std::string handle(std::string gcode_str);

    private:
        uint32_t str_to_int(std::string gcode_str);
        std::string str_to_argument(std::string gcode_str);
        std::string str_to_filepath(std::string gcode_str);

        bool file_exists(std::string filename);

        std::string list_files();
        std::string select_file(std::string gcode_str);
        std::string delete_file(std::string gcode_str);
        std::string write_file(std::string gcode_str);
        std::string save_gcode(std::string gcode_str);

};

#endif //FILEMANAGER_HPP
