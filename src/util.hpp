#ifndef UTIL_HPP
#define UTIL_HPP

#include <algorithm> 
#include <functional> 
#include <map>

#include <sstream>

template<typename TO, typename FROM>
TO lexical_cast(FROM const &x)
{
    std::stringstream os;
    TO ret;

    os << x;
    os >> ret;

    return ret;  
}

// TODO: See list_files() in FileManager
template<typename FROM, typename TO>
std::string map_to_json(std::map<FROM, TO> m)
{
    std::string str = "{";
    for(auto& kv : m)
    {
        str += kv.first;
        str += ":";
        str += kv.second;
        str += ",";
    }
    str = str.substr(0,str.length()-1);
    str += "}";

    return str;
}

// trim from start
std::string& ltrim(std::string& s);
// trim from end
std::string& rtrim(std::string& s);
// trim from both ends
std::string& trim(std::string& s);

#endif //UTIL_HPP
